
APP_PLATFORM := android-16

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := hello
LOCAL_SRC_FILES := HelloJNI.c

#TARGET_ARCH_ABI := arm64-v8a

#APP_ABI := all

APP_ABI := all


include $(BUILD_SHARED_LIBRARY)
#include $(BUILD_STATIC_LIBRARY)